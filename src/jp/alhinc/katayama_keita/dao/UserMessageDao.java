package jp.alhinc.katayama_keita.dao;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.alhinc.katayama_keita.beans.UserMessage;
import jp.alhinc.katayama_keita.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num, String startTime, String endTime, String categoryName) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("where messages.created_date between ? and ? ");
            if(!StringUtils.isEmpty(categoryName)){
            	sql.append("and category like ? ");
            }
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startTime + " 00:00:00");
            ps.setString(2, endTime + " 23:59:59");
            if(!StringUtils.isEmpty(categoryName)) {
            	ps.setString(3, "%" + categoryName + "%");
            }
            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String title = rs.getString("title");
                String text = rs.getString("text");
                String category = rs.getString("category");
                int userId = rs.getInt("user_id");
                String name = rs.getString("name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setTitle(title);
                message.setMessageText(text);
                message.setCategory(category);
                message.setUserId(userId);
                message.setName(name);
                message.setCreated_date(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}