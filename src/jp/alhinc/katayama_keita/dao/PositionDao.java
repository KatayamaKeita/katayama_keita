package jp.alhinc.katayama_keita.dao;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.alhinc.katayama_keita.beans.Position;
import jp.alhinc.katayama_keita.exception.SQLRuntimeException;

public class PositionDao {
    public List<Position> getPosition(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM positions";

            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Position> ret = toPositionList(rs);

            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Position> toPositionList(ResultSet rs) throws SQLException {

        List<Position> ret = new ArrayList<Position>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int level = rs.getInt("level");

                Position position = new Position();
                position.setId(id);
                position.setName(name);
                position.setLevel(level);

                ret.add(position);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public Position getPosition(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM positions where id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<Position> ret = toPositionList(rs);
			if (ret.isEmpty() == true) {
				return null;
			} else if (2 <= ret.size()) {
				throw new IllegalStateException("2 <= ret.size()");
			} else {
				return ret.get(0);
			}
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
