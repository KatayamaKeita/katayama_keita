package jp.alhinc.katayama_keita.dao;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.alhinc.katayama_keita.beans.Branch;
import jp.alhinc.katayama_keita.exception.SQLRuntimeException;

public class BranchDao {
    public List<Branch> getBranch(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branches";

            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Branch> ret = toBranchList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Branch> toBranchList(ResultSet rs) throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int level = rs.getInt("level");

                Branch branch = new Branch();
                branch.setId(id);
                branch.setName(name);
                branch.setLevel(level);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public Branch getBranch(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branches where id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<Branch> ret = toBranchList(rs);
			if (ret.isEmpty() == true) {
				return null;
			} else if (2 <= ret.size()) {
				throw new IllegalStateException("2 <= ret.size()");
			} else {
				return ret.get(0);
			}
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
