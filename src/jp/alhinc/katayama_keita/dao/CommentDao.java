package jp.alhinc.katayama_keita.dao;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.alhinc.katayama_keita.beans.Comment;
import jp.alhinc.katayama_keita.exception.SQLRuntimeException;

public class CommentDao {

    public int insert(Connection connection, Comment comment) {
    	int autoIncKey = -1;
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("text");
            sql.append(", user_id");
            sql.append(", message_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // text
            sql.append(", ?"); // user_id
            sql.append(", ?"); // message_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString(), java.sql.Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, comment.getText());
            ps.setInt(2, comment.getUserId());
            ps.setInt(3, comment.getMessageId());

            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
			autoIncKey = getAutoIncrementKey(rs);
			return autoIncKey;
        } catch (SQLException e) {
        	e.printStackTrace();
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	private int getAutoIncrementKey(ResultSet rs)
			throws SQLException {

		int key = -1;
		try {
			if(rs.next()) {
				key = rs.getInt(1);
			}
		} finally {
			close(rs);
		}
		return key;
	}

    public List<Comment> getComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Comment> toCommentList(ResultSet rs)
            throws SQLException {

        List<Comment> ret = new ArrayList<Comment>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String text = rs.getString("text");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("name");
                int messageId = rs.getInt("message_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                Comment comment = new Comment();
                comment.setId(id);
                comment.setText(text);
                comment.setUserId(userId);
                comment.setUserName(userName);
                comment.setMessageId(messageId);
                comment.setCreated_date(createdDate);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public int delete(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM comments ");
            sql.append("where id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, id);
            int cnt = ps.executeUpdate();

            return cnt;

        } catch (SQLException e) {
        	e.printStackTrace();
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public Comment getComment(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.text as text, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("users.name as name, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("where comments.id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<Comment> ret = toCommentList(rs);
            return ret.get(0);

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}