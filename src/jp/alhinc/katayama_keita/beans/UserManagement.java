package jp.alhinc.katayama_keita.beans;

import java.io.Serializable;
import java.util.Date;

public class UserManagement implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private String loginId;
    private String branchName;
    private String positionName;
    private int isStopped;
    private String state;
    public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	private Date createdDate;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


}