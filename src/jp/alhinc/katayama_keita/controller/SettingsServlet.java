package jp.alhinc.katayama_keita.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import jp.alhinc.katayama_keita.beans.Branch;
import jp.alhinc.katayama_keita.beans.Position;
import  jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.service.BranchService;
import jp.alhinc.katayama_keita.service.PositionService;
import  jp.alhinc.katayama_keita.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();

		try {
			User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
			if(editUser != null)
			{
			request.setAttribute("loginUser", loginUser);
			request.setAttribute("editUser", editUser);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
			}else {
				messages.add("このユーザーは存在しません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("userManagement");
			}
		}catch(NumberFormatException e) {
			messages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userManagement");
		}

	}
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException ,RuntimeException{

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
		if (isValid(request, messages) == true) {
			new UserService().update(editUser);
			response.sendRedirect("userManagement");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setName(request.getParameter("name"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

        String loginId = request.getParameter("loginId");
        User loginIdAfter = new UserService().getLoginId(loginId);
        User loginIdBefore = new UserService().getLoginIdBefore(request.getParameter("id"));
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("passwordConfirm");
        String name = request.getParameter("name");
        int branchId = Integer.parseInt(request.getParameter("branchId"));
        int positionId = Integer.parseInt(request.getParameter("positionId"));
        Branch branch = new BranchService().getBranch(branchId);
        Position position = new PositionService().getPosition(positionId);

        if(StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if(10 < name.length()) {
            messages.add("名前は10文字以下で入力してください");
        }
        if (StringUtils.isBlank(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }else if (6 > loginId.length()) {
            messages.add("ログインIDは6文字以上で入力してください");
        }else if(!loginId.matches("^[a-zA-Z0-9]+$")) {
        	messages.add("ログインIDは半角英数字で入力してください");
        }
        if(20 < loginId.length()) {
            messages.add("ログインIDは20文字以下で入力してください");
        }
        if(loginIdAfter.getLoginId() != null && !(loginId.equals(loginIdBefore.getLoginId()))) {
        	messages.add("このIDはすでに登録されています");
        }
        if(!password.matches("^[a-zA-Z0-9-/:-@\\[-\\`\\{-\\~]+$") && !StringUtils.isBlank(password)) {
        	messages.add("パスワードは半角英数字記号で入力してください");
        }
        if(!Objects.equals(password, passwordConfirm)) {
        	messages.add("パスワードと確認用パスワードが一致しません");
        }
        if(positionId == 0) {
            messages.add("部署・役職を入力してください");
        }else if(branch.getLevel() != position.getLevel()) {
        	messages.add("支店と部署・役職の組み合わせが正しくありません");
        }
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}