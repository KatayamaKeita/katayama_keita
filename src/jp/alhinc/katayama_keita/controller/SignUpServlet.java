package jp.alhinc.katayama_keita.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import jp.alhinc.katayama_keita.beans.Branch;
import jp.alhinc.katayama_keita.beans.Position;
import jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.service.BranchService;
import jp.alhinc.katayama_keita.service.PositionService;
import jp.alhinc.katayama_keita.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

	List<Branch> branches = new BranchService().getBranch();
	List<Position> positions = new PositionService().getPosition();

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        User user = new User();
        user.setLoginId(request.getParameter("login_id"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

        if (isValid(request, messages) == true) {

            new UserService().register(user);

            response.sendRedirect("userManagement");
        } else {
            session.setAttribute("errorMessages", messages);
    		request.setAttribute("branches", branches);
    		request.setAttribute("positions", positions);
            request.setAttribute("User", user);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String loginId = request.getParameter("login_id");
        User isLoginId = new UserService().getLoginId(loginId);
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("passwordConfirm");
        String name = request.getParameter("name");
        int branchId = Integer.parseInt(request.getParameter("branchId"));
        int positionId = Integer.parseInt(request.getParameter("positionId"));
        Branch branch = new BranchService().getBranch(branchId);
        Position position = new PositionService().getPosition(positionId);

        if (StringUtils.isBlank(name) == true) {
            messages.add("名前を入力してください");
        }
        if (10 < name.length()) {
            messages.add("名前は10文字以下で入力してください");
        }
        if (StringUtils.isBlank(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }else if (6 > loginId.length()) {
            messages.add("ログインIDは6文字以上で入力してください");
        }else if(!loginId.matches("^[a-zA-Z0-9]+$")) {
        	messages.add("ログインIDは半角英数字で入力してください");
        }
        if (20 < loginId.length()) {
            messages.add("ログインIDは20文字以下で入力してください");
        }
        if(isLoginId.getLoginId() != null) {
        	messages.add("このIDはすでに登録されています");
        }
        if (StringUtils.isBlank(password)) {
            messages.add("パスワードを入力してください");
        }else if (6 > password.length()) {
            messages.add("パスワードは6文字以上で入力してください");
        }else if(!Objects.equals(password, passwordConfirm)) {
        	messages.add("パスワードと確認用パスワードが一致しません");
        }
        if(!password.matches("^[a-zA-Z0-9-/:-@\\[-\\`\\{-\\~]+$") && !StringUtils.isBlank(password)) {
        	messages.add("パスワードは半角英数字記号で入力してください");
        }
        if (20 < password.length()) {
            messages.add("パスワードは20文字以下で入力してください");
        }

        if(branchId != 0 && positionId != 0 && branch.getLevel() != position.getLevel()) {
        	messages.add("支店と部署・役職の組み合わせが正しくありません");
        }
        if (branchId == 0) {
            messages.add("支店を入力してください");
        }
        if (positionId == 0) {
            messages.add("部署・役職を入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}