package jp.alhinc.katayama_keita.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.service.UserService;

@WebServlet(urlPatterns = { "/userStopped" })
public class UserStoppedServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
        protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("id")));

        if (Integer.parseInt(request.getParameter("isStopped")) == 0) {

            new UserService().stopped(user);

            response.sendRedirect("./userManagement");
        } else {
            new UserService().restoration(user);

            response.sendRedirect("./userManagement");
        }
    }

}