package jp.alhinc.katayama_keita.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.beans.UserManagement;
import jp.alhinc.katayama_keita.service.UserService;

@WebServlet(urlPatterns = { "/userManagement" })
public class UserManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User loginUser = (User) request.getSession().getAttribute("loginUser");

        List<UserManagement> users = new UserService().getUser();
        request.setAttribute("loginUser", loginUser);
        request.setAttribute("users", users);
        request.getRequestDispatcher("/userManagement.jsp").forward(request, response);
    }
}