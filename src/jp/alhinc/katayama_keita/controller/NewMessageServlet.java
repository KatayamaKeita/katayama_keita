package jp.alhinc.katayama_keita.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import jp.alhinc.katayama_keita.beans.Message;
import jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("/newMessage.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();
        User user = (User) session.getAttribute("loginUser");

        Message message = new Message();
        message.setTitle(request.getParameter("title"));
        message.setText(request.getParameter("text"));
        message.setCategory(request.getParameter("category"));
        message.setUserId(user.getId());

        if (isValid(request, errorMessages) == true) {

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", errorMessages);
            request.setAttribute("message" ,message);

            request.getRequestDispatcher("newMessage.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String title = request.getParameter("title");
        String text = request.getParameter("text");
        String category = request.getParameter("category");

        if (StringUtils.isBlank(title) == true) {
            messages.add("件名を入力してください");
        }
        if (30 < title.length()) {
            messages.add("件名は30文字以下で入力してください");
        }
        if (StringUtils.isBlank(text) == true) {
            messages.add("本文を入力してください");
        }
        if (1000 < text.length()) {
            messages.add("本文は1000文字以下で入力してください");
        }
        if (StringUtils.isBlank(category) == true) {
            messages.add("カテゴリーを入力してください");
        }
        if (10 < category.length()) {
            messages.add("カテゴリーは10文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}