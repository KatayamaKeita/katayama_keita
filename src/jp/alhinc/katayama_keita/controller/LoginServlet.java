package jp.alhinc.katayama_keita.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(loginId, password);

        HttpSession session = request.getSession();
        if (isValid(request, messages, user) == true) {
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            session.setAttribute("loginId", request.getParameter("loginId"));
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
	private boolean isValid(HttpServletRequest request, List<String> messages, User user) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDまたはパスワードが入力されていません");
		}else if (StringUtils.isEmpty(password) == true) {
			messages.add("ログインIDまたはパスワードが入力されていません");
		}else if (user == null) {
			messages.add("ログインIDまたはパスワードが誤っています");
		}


		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}