package jp.alhinc.katayama_keita.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.alhinc.katayama_keita.beans.Comment;
import jp.alhinc.katayama_keita.service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	Comment comment = createCommentData(request);
        List<String> messages = new ArrayList<String>();

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";
        if (isValid(comment, messages) == true) {
            int key = new CommentService().register(comment);
            Comment userComment = new CommentService().getComment(key);
            isSuccess = true;
            responseJson = String.format("{\"is_success\" : \"%s\", \"userComment\" : %s}", isSuccess, exportCommentToJson(userComment));
        } else {
        	errors = new ObjectMapper().writeValueAsString(messages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);
        }
		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(responseJson);
    }
	private Comment createCommentData(HttpServletRequest request) {
		String data = request.getParameter("comment");

		Comment comment = new Comment();
		try {
			comment = new ObjectMapper().readValue(data, Comment.class);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return comment;
	}
	private String exportCommentToJson(Comment userComment) {
		String json = null;

		try {
			json = new ObjectMapper().writeValueAsString(userComment);
		} catch(Exception e) {
			e.printStackTrace();
		}

		return json;
	}
    private boolean isValid(Comment comment, List<String> messages) {

        if (StringUtils.isBlank(comment.getText()) == true) {
            messages.add("コメントを入力してください");
        }
        if (500 < comment.getText().length()) {
            messages.add("コメントは500文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}