package jp.alhinc.katayama_keita.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.alhinc.katayama_keita.beans.Comment;
import jp.alhinc.katayama_keita.service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String id = request.getParameter("id");
		List<String> errorMessages = new ArrayList<String>();

		boolean isSuccess = false;
		String responseJson = "";
		String errors = "";

		if(isValid(id, errorMessages)) {
			/* バリデーション成功 */
			// 削除処理(cntには削除した件数が入る)
			int cnt = new CommentService().delete(Integer.parseInt(id));
			// 成功情報を返す
			isSuccess = true;
			responseJson = String.format("{\"is_success\" : \"%s\"}", isSuccess);
		} else {
			/* バリデーションエラー */
			// ListをJsonの形にして返す
			errors = new ObjectMapper().writeValueAsString(errorMessages);
			responseJson = String.format("{\"is_success\" : \"%s\", \"errors\" : %s}", isSuccess, errors);
		}

		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().write(responseJson);
	}
	/*
	 * 入力データの検証を行うメソッド
	 * 複数のエラー文が変えることはないので、毎回return falseする
	 */
	private boolean isValid(String id, List<String> errorMessages) {
		// idが存在し、かつ数字であること
		if(StringUtils.isEmpty(id) || !id.matches("^[0-9]+$")) {
			errorMessages.add("データに問題が発生しました");
			return false;
		}
		int commentId = Integer.parseInt(id);
		// コメントが存在するか
		Comment userComment = new CommentService().getComment(commentId);
		if(userComment == null) {
			errorMessages.add("そのコメントは存在していません");
			return false;
		}
		// コメントの登録者がログインユーザ自身か


		if(errorMessages.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

}