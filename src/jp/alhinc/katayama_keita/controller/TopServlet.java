package jp.alhinc.katayama_keita.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import jp.alhinc.katayama_keita.beans.Comment;
import jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.beans.UserMessage;
import jp.alhinc.katayama_keita.service.CommentService;
import jp.alhinc.katayama_keita.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    String startTimeDefault = "2000-01-01";
    String endTimeDefault = "2050-12-31";

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	String startTime = startTimeDefault;
        String endTime = endTimeDefault;
        String categoryName = request.getParameter("category");

        if(!StringUtils.isEmpty(request.getParameter("startTime"))) {
        	startTime = request.getParameter("startTime");
        	request.setAttribute("startTime",startTime);
        }
        if(!StringUtils.isEmpty(request.getParameter("endTime"))) {
        	endTime = request.getParameter("endTime");
        	request.setAttribute("endTime",endTime);
        }

        User user = (User) request.getSession().getAttribute("loginUser");
        List<UserMessage> messages = new MessageService().getMessage(startTime ,endTime , categoryName);
        List<Comment> comments = new  CommentService().getComment();

        request.setAttribute("loginUser", user);
        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("categoryName", categoryName);
        request.getRequestDispatcher("/top.jsp").forward(request, response);
    }
}