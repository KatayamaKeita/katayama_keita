package jp.alhinc.katayama_keita.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.alhinc.katayama_keita.beans.Message;
import jp.alhinc.katayama_keita.service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

        Message message = new Message();
        message.setId(Integer.parseInt(request.getParameter("id")));
        new MessageService().delete(message);
        response.sendRedirect("./");
	}

}