package jp.alhinc.katayama_keita.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.katayama_keita.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

		List<String> messages = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest) req).getSession();
		User user = (User) session.getAttribute("loginUser");
		if(user == null && !(((HttpServletRequest)req).getServletPath().equals("/login")) && !(((HttpServletRequest)req).getServletPath().equals("/css/style.css"))){
			((HttpServletResponse)res).sendRedirect("login");
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			return;
		}

		chain.doFilter(req, res);
	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}