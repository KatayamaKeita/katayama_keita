package jp.alhinc.katayama_keita.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.alhinc.katayama_keita.beans.User;

@WebFilter(urlPatterns = { "/userManagement", "/settings", "/signup" })
public class RightFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{

		HttpSession session = ((HttpServletRequest) req).getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();

		if(user.getPositionId() != 1 && user.getPositionId() != 2){
			((HttpServletResponse)res).sendRedirect("./");
			messages.add("権限がありません");
			session.setAttribute("errorMessages", messages);
			return;
		}
		chain.doFilter(req, res);
	}

	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}
}