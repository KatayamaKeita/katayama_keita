package jp.alhinc.katayama_keita.service;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;
import static jp.alhinc.katayama_keita.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import jp.alhinc.katayama_keita.beans.Position;
import jp.alhinc.katayama_keita.dao.PositionDao;

public class PositionService {
    public List<Position> getPosition() {

        Connection connection = null;
        try {
            connection = getConnection();

            PositionDao positionDao = new PositionDao();
            List<Position> ret = positionDao.getPosition(connection);
            commit(connection);

            return ret;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public Position getPosition(int id) {

        Connection connection = null;
        try {
            connection = getConnection();

            PositionDao positionDao = new PositionDao();
            Position position = positionDao.getPosition(connection,id);

            commit(connection);

            return position;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
