package jp.alhinc.katayama_keita.service;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;
import static jp.alhinc.katayama_keita.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.beans.UserManagement;
import jp.alhinc.katayama_keita.dao.UserDao;
import jp.alhinc.katayama_keita.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();
            if(!StringUtils.isEmpty(user.getPassword())) {
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);
            }

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User getUser(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            User user = userDao.getUser(connection, userId);

            commit(connection);

            return user;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void update(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            if(!StringUtils.isEmpty(user.getPassword())) {
            	String encPassword = CipherUtil.encrypt(user.getPassword());
            	user.setPassword(encPassword);
            }
            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<UserManagement> getUser() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao managementDao = new UserDao();
            List<UserManagement> ret = managementDao.getUsers(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void stopped(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.stopped(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }    public void restoration(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.restoration(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User getLoginId(String loginId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao idSelect = new UserDao();
            User loginIdAfter = idSelect.getUserId(connection, loginId, LIMIT_NUM);

            commit(connection);

            return loginIdAfter;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public User getLoginIdBefore(String id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao idSelect = new UserDao();
            User loginIdBefore = idSelect.getUserIdBefore(connection, id, LIMIT_NUM);

            commit(connection);

            return loginIdBefore;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}