package jp.alhinc.katayama_keita.service;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;
import static jp.alhinc.katayama_keita.utils.DBUtil.*;

import java.sql.Connection;

import jp.alhinc.katayama_keita.beans.User;
import jp.alhinc.katayama_keita.dao.UserDao;
import jp.alhinc.katayama_keita.utils.CipherUtil;

public class LoginService {

    public User login(String loginId, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, loginId, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}