package jp.alhinc.katayama_keita.service;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;
import static jp.alhinc.katayama_keita.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import jp.alhinc.katayama_keita.beans.Comment;
import jp.alhinc.katayama_keita.dao.CommentDao;

public class CommentService {

    public int register(Comment comment) {

        Connection connection = null;
        try {
        	connection = getConnection();
            int key = new CommentDao().insert(connection, comment);

            commit(connection);
            return key;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<Comment> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            List<Comment> ret = commentDao.getComments(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public int delete(int id) {

        Connection connection = null;
        try {
            connection = getConnection();

            int cnt = new CommentDao().delete(connection, id);

            commit(connection);

            return cnt;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public Comment getComment(int id) {
		Connection connection = null;
		try {
			connection = getConnection();

			Comment userComment = new CommentDao().getComment(connection, id);

			commit(connection);

			return userComment;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}