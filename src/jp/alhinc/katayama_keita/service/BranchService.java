package jp.alhinc.katayama_keita.service;

import static jp.alhinc.katayama_keita.utils.CloseableUtil.*;
import static jp.alhinc.katayama_keita.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import jp.alhinc.katayama_keita.beans.Branch;
import jp.alhinc.katayama_keita.dao.BranchDao;

public class BranchService {
    public List<Branch> getBranch() {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            List<Branch> ret = branchDao.getBranch(connection);

            commit(connection);

            return ret;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public Branch getBranch(int id) {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            Branch branch = branchDao.getBranch(connection,id);

            commit(connection);

            return branch;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
