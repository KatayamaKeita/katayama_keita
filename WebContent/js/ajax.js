$(function() {
	$('button#register').on('click', function() {
		// 送信するデータを用意
		var form = $(this).parents("form#register-form");
		var messageId = form.find('#messageId').val();
		var userId = form.find('#userId').val();
		var text = form.find('#text').val();
		var comment = { 'messageId': messageId, 'userId': userId, 'text': text };
		console.log("1");
		// 全てのerror-areaのメッセージを削除する
		$(".error-area").each(function(k, v){ $(v).find("ul").empty(); })

		// Ajax通信処理
		$.ajax({
			dataType: 'json',
			type: "POST",
			url: 'newComment',
			data: { comment: JSON.stringify(comment) }
		}).done(function(data, textStatus, jqXHR) {
			// 成功時の処理
			if(data.is_success == 'true') {
				/* バリデーション通過 */
				// コメントデータを追加
				$(function(){form.parents("div.comment-area").find("div.view-area").append(createCommentBlock(data.userComment));
				})

				// 全てのtextarea要素の中身を削除
				$(".comment-text-area").each(function(k, v) { $(this).val(""); })
				console.log("2");
			} else {
				/* バリデーションエラー */
				var errorArea = form.parents("div.comment-area").find("div.error-area");
				// 入力フォームの上にエラーメッセージを表示させる
				data.errors.forEach(function(v, k) {
					// エラーメッセージの要素の数だけ表示させる
					errorArea.find("ul").append("<li>" + v + "</li>");
				});
			}
		}).fail(function(data, textStatus, jqXHR) {
			// 通信失敗時の処理
			console.log(data);
			console.log('error!!');
		});
	});

	/*
	 * 表示用データの作成
	 */
	function createCommentBlock(userComment) {
		date = new Date(userComment.created_date);
		createdAt = date.getFullYear() + '/' + ('0' + (date.getMonth()+1)).slice(-2) + '/' + ('0' + date.getDate()).slice(-2) + " " +
					('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
		body = "<div class='comment'>" +
				"<div class='text'>" + userComment.text.replace( /\r?\n/g, '<br />' ) + "</div>" +
				"<input type='hidden' name='id' value=" + userComment.id + " id='comment_id'>" +
				"<div class='info'><div class='name'>投稿者：" + userComment.userName + "</div>" +
				"<div class='date'>投稿日時：" + createdAt + "</div><br/>"+
				"<button type='button' id='delete'>投稿削除</button></div>" +
				"</div>";
		return body;
	}
});
/*
 * コメント削除
 */
$(document).on("click", "button#delete", function(){
	// 削除するコメントのIDを取得
	var commentBlock = $(this).parents("div.comment");
	var id = commentBlock.find('#comment_id').val();

	// 全てのerror-areaのメッセージを削除する
	$(".error-area").each(function(k, v){ $(v).find("ul").empty(); })

	// Ajax通信処理
	$.ajax({
		dataType: 'json',
		type: "POST",
		url: 'deleteComment',
		data: { id: id }
	}).done(function(data, textStatus, jqXHR) {
		// 成功時の処理
		if(data.is_success == 'true') {
			/* バリデーション通過 */
			// 画面上のコメントを削除
			commentBlock.remove();
		} else {
			/* バリデーションエラー */
		}
	}).fail(function(data, textStatus, jqXHR) {
		// 通信失敗時の処理
		console.log('error!!');
	});
});
