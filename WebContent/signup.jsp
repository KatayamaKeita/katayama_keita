<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">

function positionSet(){
	var branchId = document.getElementById("branchId").value;
	var position = document.getElementById("positionId");
    switch (branchId){
      case "1":
    	  document.parameter.positionId.options[1].disabled = false;
    	  document.parameter.positionId.options[2].disabled = false;
    	  document.parameter.positionId.options[3].disabled = true;
    	  document.parameter.positionId.options[4].disabled = true;
    	  break;
      case "2":
      case "3":
    	  document.parameter.positionId.options[1].disabled = true;
    	  document.parameter.positionId.options[2].disabled = true;
    	  document.parameter.positionId.options[3].disabled = false;
    	  document.parameter.positionId.options[4].disabled = false;
    	  break;
  }
}
window.onload = function load(){
	positionSet();
}
function positionChange(){
	var branchId = document.getElementById("branchId").value;
	var position = document.getElementById("positionId");
    switch (branchId){
      case "1":
    	  document.parameter.positionId.options[1].disabled = false;
    	  document.parameter.positionId.options[2].disabled = false;
    	  document.parameter.positionId.options[3].disabled = true;
    	  document.parameter.positionId.options[4].disabled = true;
    	  break;
      case "2":
      case "3":
    	  document.parameter.positionId.options[1].disabled = true;
    	  document.parameter.positionId.options[2].disabled = true;
    	  document.parameter.positionId.options[3].disabled = false;
    	  document.parameter.positionId.options[4].disabled = false;
    	  break;
  }
	document.parameter.positionId.selectedIndex=0;
}
</script>
    </head>
    <body>
        <div class="main-contents">
				<div class="header">
					<ul class="navi">
						<li class="active"><a href="./">ホーム</a></li>
						<li><a href="newMessage">新規投稿</a></li>
						<li><a href="userManagement">ユーザー管理</a></li>
						<li class="right"><a href="logout">ログアウト</a></li>
					</ul>
				</div>
				<c:if test="${ not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages}" var="message">
								<li><c:out value="${message}" /></li>
							</c:forEach>
						</ul>
					<c:remove var="errorMessages" scope="session" />
					</div>
				</c:if>
			<div class="user-form">
            <form action="signup" method="post" name="parameter">
                <br />
                <label for="name">名前</label> <input name="name" value="${User.name}" id="name" /><br><br>
                <label for="login_id">ログインID</label> <input name="login_id" value="${User.loginId}"id="login_id" /><br><br>
                <label for="password">パスワード</label> <input name="password" type="password" id="password" /><br><br>
                <label for="password">確認用パスワード</label><input name="passwordConfirm" type="password" id="pass_confirm" /><br><br>
                <label for="branch">支店</label>
                <select name="branchId" id="branchId" onChange="positionChange()">
                <option value="0">選択してください</option>
	            	<c:forEach items="${branches}" var="branch">
                		<c:choose>
                			<c:when test="${branch.id == User.branchId}">
                		<option value="${branch.id}" selected>${branch.name}</option>
                		</c:when>
                		<c:otherwise>
                			<option value="${branch.id}">${branch.name}</option>
                		</c:otherwise>
                		</c:choose>
					</c:forEach>
				</select>
                <br><br>
                <label for="position">部署・役職</label>
                <select name="positionId">
                	<option value="0">選択してください</option>
	                <c:forEach items="${positions}" var="position">
	                	<c:choose>
			         		<c:when test="${position.id == User.positionId}">
        	   					<option value="${position.id}" selected>${position.name}</option>
           					</c:when>
           					<c:otherwise>
           						<option value="${position.id}">${position.name}</option>
           					</c:otherwise>
           				</c:choose>
						</c:forEach>
                </select>
                <br>
                <br /> <input type="submit" value="登録" /><br><br><a href="userManagement">戻る</a><br>
            </form>
            </div>
            <div class="copyright">Copyright(c)Katayama.Keita</div>
        </div>
    </body>
</html>