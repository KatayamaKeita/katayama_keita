<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>投稿画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<ul class="navi">
				<li class="active"><a href="./">ホーム</a></li>
				<li><a href="newMessage">新規投稿</a></li>
				<c:if test="${loginUser.positionId == 1 || loginUser.positionId == 2}"><li><a href="userManagement">ユーザー管理</a></li></c:if>
				<li class="right"><a href="logout">ログアウト</a></li>
			</ul>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="form-area">
			<form action="newMessage" method="post"><br/>
				<label for="title">件名<br /></label>
				<input name="title"value="${message.title}" id="title" />
				<br/> 本文<br />
				<textarea name="text" class="text-box">${message.text}</textarea><br />
				<label for="category">カテゴリー<br /></label>
				<input name="category" value ="${message.category}" id="category" /> <br /> <br />
				<input type="submit" value="登録" />
			</form><br><br>
			<a href="./">戻る</a>
		</div>
		<div class="copyright">Copyright(c)Katayama.Keita</div>
	</div>
</body>
</html>
