<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="./js/ajax.js"></script>
<script type="text/javascript">
function check(){
	if(window.confirm('削除しますか？')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}
function lineChange(){
	text.replace(/\n/g, '<br>');
}
</script>
</head>
<body>
<div class="main-contents">
	<div class="header">
		<ul class="navi">
			<li class="active"><a href="./">ホーム</a></li>
			<li><a href="newMessage">新規投稿</a></li>
			<c:if test="${loginUser.positionId == 1 || loginUser.positionId == 2}"><li><a href="userManagement">ユーザー管理</a></li></c:if>
			<li class="right"><a href="logout">ログアウト</a></li>
		</ul>
	</div><br />
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" /></li>
				</c:forEach>
			</ul>
			<c:remove var="errorMessages" scope="session" />
		</div>
	</c:if>
	<div class="Narrowing">
		 投稿を検索<br/>
		<form action="./" method="get">
			<div class="date">
				<input type="date" name="startTime" value="${startTime}"> ～<input type="date" name="endTime" value="${endTime}"><br/>
				カテゴリー<input name="category" value="${categoryName}">
				<input type="submit" value="検索"><br />
			</div>
		</form>
	</div>
	<div class="messages">
		<c:forEach items="${messages}" var="message">
			<c:set var="messageText" value="${message.messageText}" />
			<div class="message">
				<div class="text">
					<div class="title">
						<c:out value="${message.title}" />
					</div><br/>
					<c:forEach var="text" items="${fn:split(messageText,'
')}" ><c:out value="${text}" /><br></c:forEach>
				</div>
				<div class="info">
					<div class="category">カテゴリー：<c:out value="${message.category}" /></div>
					<div class="account-name">
						<span class="name">投稿者：<c:out value="${message.name}" /></span><br/>
					</div>
					<div class="date">
						投稿日時：<fmt:formatDate value="${message.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" /><br/>
					</div>
					<c:if test="${message.userId == loginUser.id}">
						<div class="deleteMessage">
							<form action="deleteMessage" method="post" onSubmit="return check()">
								<input type="hidden" name="id" value="${message.id}">
								<input type="submit" value="投稿削除" >
							</form>
						</div>
					</c:if>
				</div><br/>
				<c:forEach items="${comments}" var="comment">
					<c:if test="${comment.messageId == message.id }">
						<div class="comment">
							<div class="text">
								<c:forEach var="commentText" items="${fn:split(comment.text,'
')}" ><c:out value="${commentText}" /><br></c:forEach>
							</div>
							<div class="info">
								<div class="name">投稿者：<c:out value="${comment.userName}"></c:out></div>
								<div class="date">投稿日時：<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div><br/>
								<c:if test="${comment.userId == loginUser.id}">
									<input type="hidden" name="id" value="${comment.id}" id='comment_id'>
									<button type='button' id='delete'>投稿削除</button>
								</c:if>
							</div>
						</div>
					</c:if>
				</c:forEach>
				<div class='comment-area'>
					<div class='view-area'>
					</div>
					<div class='error-area'>
						<ul></ul>
					</div>
				<div class="commentFormat">
					<form id='register-form'>
						<input type="hidden" id="messageId" value="${message.id}">
						<input type="hidden" id="userId" value="${loginUser.id}">
						コメント<br/>
						<textarea id="text"class='comment-text-area'></textarea><br/>
						<button type='button' id='register'>登録</button><br/><br/>
					</form>
				</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<div class="copyright">Copyright(c)Katayama.Keita</div>
</div>
</body>
</html>