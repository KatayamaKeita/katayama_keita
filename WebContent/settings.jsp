<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${editUser.name}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
        <script type="text/javascript">

function positionSet(){
	var branchId = document.getElementById("branchId").value;
	var position = document.getElementById("positionId");
    switch (branchId){
      case "1":
    	  document.parameter.positionId.options[1].disabled = false;
    	  document.parameter.positionId.options[2].disabled = false;
    	  document.parameter.positionId.options[3].disabled = true;
    	  document.parameter.positionId.options[4].disabled = true;
    	  break;
      case "2":
      case "3":
    	  document.parameter.positionId.options[1].disabled = true;
    	  document.parameter.positionId.options[2].disabled = true;
    	  document.parameter.positionId.options[3].disabled = false;
    	  document.parameter.positionId.options[4].disabled = false;
    	  break;
  }
}
window.onload = function load(){
	positionSet();
}
function positionChange(){
	var branchId = document.getElementById("branchId").value;
	var position = document.getElementById("positionId");
    switch (branchId){
      case "1":
    	  document.parameter.positionId.options[1].disabled = false;
    	  document.parameter.positionId.options[2].disabled = false;
    	  document.parameter.positionId.options[3].disabled = true;
    	  document.parameter.positionId.options[4].disabled = true;
    	  break;
      case "2":
      case "3":
    	  document.parameter.positionId.options[1].disabled = true;
    	  document.parameter.positionId.options[2].disabled = true;
    	  document.parameter.positionId.options[3].disabled = false;
    	  document.parameter.positionId.options[4].disabled = false;
    	  break;
  }
	document.parameter.positionId.selectedIndex=0;
}
		</script>
    </head>
    <body>
    	<input type="hidden" id="loginUserId" value="${loginUser.id}"/>
        <div class="main-contents">
		<div class="header">
			<ul class="navi">
				<li class="active"><a href="./">ホーム</a></li>
				<li class="right"><a href="logout">ログアウト</a></li>
			</ul>
		</div>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
            <div class="user-form">
            <form action="settings" method="post" name="parameter"><br />
            	<input type="hidden" name="id" id="id" value="${editUser.id}">

                <label for="name">ユーザー名</label>
                <input name="name" value="${editUser.name}" id="name"/><br><br>

            	<label for="loginId">ログインID</label>
                <input name="loginId" value="${editUser.loginId}" id="loginId"><br><br>

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/><br><br>

                <label for="password">確認用パスワード</label>
                <input name="passwordConfirm" type="password" id="passwordConfirm" /><br><br>

                <label for="branch">支店</label>
                <c:choose>
                	<c:when test="${loginUser.id == editUser.id}">
                		<input type="hidden" value="${editUser.branchId}" name="branchId"/>
                		<c:out value="${editUser.branchName}"/>
                	</c:when>
                	<c:otherwise>
                		<select name="branchId" id="branchId" onChange="positionChange()">
	                		<c:forEach items="${branches}" var="branch">
                				<c:choose>
                					<c:when test="${branch.id == editUser.branchId}">
                						<option value="${branch.id}" selected>${branch.name}</option>
                					</c:when>
                					<c:otherwise>
                						<option value="${branch.id}">${branch.name}</option>
                					</c:otherwise>
                				</c:choose>
							</c:forEach>
						</select>
					</c:otherwise>
                </c:choose>
                <br/><br/>

                <label for="position">部署・役職</label>
                <c:choose>
                	<c:when test="${loginUser.id == editUser.id}">
                		<input type="hidden" value="${editUser.positionId}" name="positionId"/>
                		<c:out value="${editUser.positionName}"/>
                	</c:when>
                	<c:otherwise>
		                <select name="positionId" id="positionId">
		                <option value="0">-</option>
			                <c:forEach items="${positions}" var="position">
		                	<c:choose>
				         		<c:when test="${position.id == editUser.positionId}">
                					<option value="${position.id}" selected>${position.name}</option>
                				</c:when>
                				<c:otherwise>
                					<option value="${position.id}">${position.name}</option>
                				</c:otherwise>
                			</c:choose>
							</c:forEach>
                		</select>
               		</c:otherwise>
                </c:choose>
                <br/><br/>

                <input type="submit" value="登録" /><br><br>
                <a href="userManagement">戻る</a>
            </form>
            </div>
            <div class="copyright"> Copyright(c)Katayama.Keita</div>
        </div>
    </body>
</html>