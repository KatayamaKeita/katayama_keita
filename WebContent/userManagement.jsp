<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">

<c:choose>
	<c:when test="${user.isStopped == 0}">
		<c:set var="isStopped"  value="停止"/>
	</c:when>
	<c:otherwise>
		<c:set var="isStopped"  value="復活"/>
	</c:otherwise>
</c:choose>
<script type="text/javascript">
function stopped(){
	if(window.confirm('停止しますか？')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}
function restoration(){
	if(window.confirm('復活しますか？')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}
</script>
</head>
<body>
	<div class="main-contents">
			<div class="header">
				<ul class="navi">
					<li class="active"><a href="./">ホーム</a></li>
					<li><a href="newMessage">新規投稿</a></li>
					<li><a href="signup">新規ユーザー登録</a>
					<li class="right"><a href="logout">ログアウト</a></li>
				</ul>
			</div>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="users">
					<table>
						<tr>
							<th>ユーザーID</th>
							<th>ユーザー名</th>
							<th>支店名</th>
							<th>役職・部署名</th>
							<th>状態</th>
							<th>管理</th>
							<th>編集</th>
						</tr>
						<c:forEach items="${users}" var="user">
							<c:choose>
								<c:when test="${user.isStopped == 0}">
									<c:set var="isStopped"  value="停止"/>
								</c:when>
								<c:otherwise>
									<c:set var="isStopped"  value="復活"/>
								</c:otherwise>
							</c:choose>
						<tr>
							<td><c:out value="${user.loginId}" /></td>
							<td><c:out value="${user.name}" /></td>
							<td><c:out value="${user.branchName}" /></td>
							<td><c:out value="${user.positionName}" /></td>
							<td><c:out value="${user.state}" /></td>
							<td><c:choose>
									<c:when test="${loginUser.id == user.id}">
										ログイン中
									</c:when>
									<c:otherwise>
										<input type="hidden" name="id" value="${user.id}">
										<input type="hidden" name="isStopped" value="${user.isStopped}">
											<c:choose>
												<c:when test="${user.isStopped == 0}">
													<form action="userStopped" method="post" onSubmit="return stopped()">
														<input type="hidden" name="id" value="${user.id}">
														<input type="hidden" name="isStopped" value="${user.isStopped}">
														<input type="submit" value="停止" id="isStoped" >
													</form>
												</c:when>
												<c:otherwise>
														<form action="userStopped" method="post" onSubmit="return restoration()">
															<input type="hidden" name="id" value="${user.id}">
															<input type="hidden" name="isStopped" value="${user.isStopped}">
															<input type="submit" value="復活" id="isWorking">
														</form>
												</c:otherwise>
											</c:choose>
									</c:otherwise>
								</c:choose>
							</td>
							<td><a href="./settings?id=${user.id}">
									<input type="hidden" name="id" value="${user.id}">
									<input type="button" value="編集">
								</a>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		<div class="copyright">Copyright(c)Katayama.Keita</div>
	</div>
</body>
</html>